#  How To Build MediaQ API?
## Prerequisites
You need the following installed and available in your $PATH:

* [Java 7](http://java.oracle.com)

* [Apache maven 3.0.3 or greater](http://maven.apache.org/)
 
### OS X Users
Don't forget to install Java 7. You probably have 1.6 or 1.8.

Export JAVA_HOME in order to user proper Java version:
```
export JAVA_HOME=`/usr/libexec/java_home -v 1.7`
export PATH=${JAVA_HOME}/bin:$PATH
```

## To generate a sample client library
You can build a client against the swagger sample [petstore](http://petstore.swagger.io) API as follows:

```
./bin/python-mediaq.sh
```

This will run the generator with this command:

```
java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
  -i modules/swagger-codegen/src/test/resources/mediaq.json \
  -l python \
  -o out/client/python
```



You can create client generator script for other languages, too:
```
./bin/php-mediaq.sh
```

Supported config options can be different per language. Running `config-help -l {lang}` will show available options.

```
java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar config-help -l java
```

Output

```
CONFIG OPTIONS
	modelPackage
	    package for generated models

	apiPackage
	    package for generated api classes

	invokerPackage
	    root package for generated code

	groupId
	    groupId in generated pom.xml

	artifactId
	    artifactId in generated pom.xml

	artifactVersion
	    artifact version in generated pom.xml

	sourceFolder
	    source folder for generated code
```

Your config file for java can look like

```
{
    "modelPackage":"models",
    "apiPackage":"apis",
    "invokerPackage":"SwaggerMediaq",
    "groupId":"edu.usc.imsc",
    "artifactId":"mediaq-api",
    "artifactVersion":"1.0.0",
    "sourceFolder":"SwaggerMediaq"
}
```

## More Information about Swagger Code Generator
This is the swagger codegen project, which allows generation of client libraries automatically from a Swagger-compliant server.  

Check out [Swagger-Spec](https://github.com/swagger-api/swagger-spec) for additional information about the Swagger project, including additional libraries with support for other languages and more. 


