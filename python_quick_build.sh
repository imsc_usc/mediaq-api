# ./vendor/bin/swagger ./application/controllers/api/ -o ./docs/json/
cp -rf "/workspaces/MediaQ workspace/mediaq-web-v3/MediaQ_MVC_V3/docs/json/swagger.json" ~/mediaq-api/modules/swagger-codegen/src/test/resources/mediaq.json
rm -rf out/client/python/SwaggerMediaq-python
./bin/python-mediaq.sh
zip -r ../mediaq-clipy/SwaggerMediaq-python.zip out/client/python/SwaggerMediaq-python
cp -rf out/client/python/SwaggerMediaq-python/setup.py ../mediaq-clipy/
cp -rf out/client/python/SwaggerMediaq-python/SwaggerMediaq ../mediaq-clipy/
